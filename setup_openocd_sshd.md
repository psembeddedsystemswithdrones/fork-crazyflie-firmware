# Center for Project Based Learning - 
# NOT FOR STUDENT EXERCISES. REFER TO readme.md

## Prepare the Apptainer Image

From the `openocd.def` file create the `.sif` image which contains
openocd, cross-compiler and a OpenSSH server. The idea is to start a
targeted OpenSSH instance configured per user with specific SSH keys for
security, and to start the debugger via SSH from visualstudio code on
the host.

## Prepare the SSH configuration per user (client)

Create a password-less SSH dedicated for this purpose:

```
mkdir -p ~/.ssh/bitcraze
# create key
ssh-keygen -f ~/.ssh/bitcraze/bitcraze_client -t ed25519 -N '' -C 'bitcraze-client'
# authorized keys
cp ~/.ssh/bitcraze/bitcraze_client.pub ~/.ssh/bitcraze/authorized_keys
```

## Configure SSH sshd Server configuration

Create a SSH host key pair for the server:

```
# create host key
ssh-keygen -f ~/.ssh/bitcraze/bitcraze_server -t ed25519 -N '' -C 'bitcraze-server'
# generate specific ssh_known_hosts file
echo "[127.0.0.1]:2222 ssh-ed25519" $(awk ' { print $2 }' ~/.ssh/bitcraze/bitcraze_server.pub) > ~/.ssh/bitcraze/ssh_known_hosts
```

## Start the Apptainer SSH Server

For Debugging within Apptainer Image:
```
apptainer shell ./openocd_sshd.sif
/usr/sbin/sshd -o Port=2222 -o AuthorizedKeysFile=~/.ssh/bitcraze/authorized_keys -o UsePAM=no -o HostKey=~/.ssh/bitcraze/bitcraze_server -d
```

Production:
```
apptainer run ./openocd_sshd.sif
```

## Visual Studio Code connection

```
ssh -i ~/.ssh/bitcraze/bitcraze_client -o UserKnownHostsFile=~/.ssh/bitcraze/ssh_known_hosts  127.0.0.1 -p 2222
```

Choose a custom config in `~/.ssh/bitcraze/config` to save the
configuration.
