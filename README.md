# P&S Embedded systems with drones


## Getting started with Apptainer Singularity

Make a project folder in which the bitcraze.sif environment, the crazyflie-clients-python, and the crazyflie-firmware are present. You can Ddownload the bitcraze.sif image from polybox.

```
git clone https://github.com/bitcraze/crazyflie-clients-python --branch 2023.11 --single-branch
```

```
git clone https://git.ee.ethz.ch/psembeddedsystemswithdrones/fork-crazyflie-firmware
cd fork-crazyflie-firmware
git checkout master
git submodule init
git submodule update
```
to jump among exercises you can use the command below, change labname with the exercise you want to build
```
git checkout labname
```
for example, use this command to have access to this readme and relevant config files:
```
git checkout main
```
or this to access lab1 code
```
git checkout lab1_task
```


## Getting started with Apptainer Singularity

Download the 'bitcraze.sif' file from https://polybox.ethz.ch/index.php/apps/files/?dir=/Shared/P%26S_UAV_Bitcraze/Students&fileid=3538928248 and put in the project folder.


Open a separate terminal (different from the one used for git)
to open the bitcraze.sif environment run in the terminal
```
apptainer shell bitcraze.sif
```
from now on all the commands will be executed in the virtual environment "Apptainer>"

to build the crazyflie firmware

"Apptainer>"
```
cd fork-crazyflie-firmware
make cf2_defconfig
make -j 4
```

to load the firmware via radio

"Apptainer>"
```
make cload
```

to install the crazyflie client (pre-installed in bitcraze.sif). Note, pip upgrade could fail, in this case skip the line. 

"Apptainer>"
```
cd crazyflie-clients-python
pip3 install --upgrade pip
pip3 install -e .
cfclient
```

to launch the crazyflie client 

"Apptainer>"
```
cfclient
```

## Getting started with Debugging

The idea is to start a targeted OpenSSH instance configured per user with specific SSH keys for security, and to start the debugger via SSH from visualstudio code on the host.

### Prepare the SSH configuration per user (client)

Open a separate terminal

Create a password-less SSH dedicated for this purpose:

```
mkdir -p ~/.ssh/bitcraze
```
create key
```
ssh-keygen -f ~/.ssh/bitcraze/bitcraze_client -t ed25519 -N '' -C 'bitcraze-client'
```
authorized keys
```
cp ~/.ssh/bitcraze/bitcraze_client.pub ~/.ssh/bitcraze/authorized_keys
```

### Configure SSH sshd Server configuration

Create a SSH host key pair for the server:


create host key
```
ssh-keygen -f ~/.ssh/bitcraze/bitcraze_server -t ed25519 -N '' -C 'bitcraze-server'
```
generate specific ssh_known_hosts file
```
echo "[127.0.0.1]:2222 ssh-ed25519" $(awk ' { print $2 }' ~/.ssh/bitcraze/bitcraze_server.pub) > ~/.ssh/bitcraze/ssh_known_hosts
```

### Start the Apptainer SSH Server

For Debugging within Apptainer Image:
```
apptainer shell bitcraze.sif
/usr/sbin/sshd -o Port=2222 -o AuthorizedKeysFile=~/.ssh/bitcraze/authorized_keys -o UsePAM=no -o HostKey=~/.ssh/bitcraze/bitcraze_server -d
```

### Visual Studio Code connection

```
ssh -i ~/.ssh/bitcraze/bitcraze_client -o UserKnownHostsFile=~/.ssh/bitcraze/ssh_known_hosts  127.0.0.1 -p 2222
```

Choose a custom config in `~/.ssh/bitcraze/config` to save the configuration.


# Private fork Update from Bitcraze

Theory:

your git repository can have more than one remote server;
In this case we want to have two:

  1. one for our private repository on _gitlab_ (will be the default one, called `origin`)
  2. one to be connected to the source repo on _github_, to be able to pull new changes (will be called `upstream`)


How to make a private fork from github to gitlab

```sh
  # 1. clone the github project in your workspace
  git clone git@github.com:whatever/repo.git

  # 2. rename the remote
  git remote rename origin upstream

  # 3. Create a new repo in gitlab website

  # 4. Add the new origin to your repo

  git remote add origin git@gitlab.extranet.weborama.com:whatever/swiper.git

  # 5. push to the private repository (track master)

  git push -u origin master
```

To push to gitlab / master, just use

```sh
   git push
```

To retrieve updates from github, use

```sh
   git pull upstream master
```

